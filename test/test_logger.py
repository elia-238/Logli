from logli import get_logger
from pprint import pprint
import os
import json


def test_server(server_client, caplog):
    for i in range(3):
        r = server_client.post(
            '/login',
            json={
                'username': 'God',
                'password': f'Password{i}'
            }
        )
        assert r.status_code == 200
    r = server_client.get('/logout')
    assert r.status_code == 200
    for log in [
        'Oops ',
        'I did again',
        'I played with your heart',
        'Got lost in the game',
        'Oh baby, baby',
        'Oops, you think I\'m in love'
    ]:
        assert log in caplog.text


def test_logging():
    log = get_logger('lib')
    other = get_logger('other')

    # Log a few different events.
    print("\n\nConsole logs\n\n")
    log.info('some information')
    log.debug('low-level debugging info')
    log.warning('important information')
    log.error('an error')
    other.error('braaa')
    try:
        raise AssertionError('critical')
    except AssertionError as exc:
        log.exception('some exception', exc=exc)

    # Read the logfile, which contains one JSON string per entry.
    with open('logfile.log') as log:
        log_json = log.read()
    log = [json.loads(entry) for entry in log_json.splitlines()]
    print("\n\nJSON logfile\n\n")
    pprint(log)

    # Trash logfile.
    os.remove('logfile.log')
