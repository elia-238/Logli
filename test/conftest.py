import pytest

from api import app


@pytest.fixture
def server_client():
    client = app.test_client()
    return client
