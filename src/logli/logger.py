import sys
import logging

import structlog


class ProcessorFormatter(logging.Formatter):
    """Custom stdlib logging formatter for structlog ``event_dict`` messages.
    Apply a structlog processor to the ``event_dict`` passed as
    ``LogRecord.msg`` to convert it to loggable format (a string).
    """

    def __init__(self, processor, fmt=None, datefmt=None, style='%'):
        """"""
        super().__init__(fmt=fmt, datefmt=datefmt, style=style)
        self.processor = processor

    def format(self, record):
        """Extract structlog's ``event_dict`` from ``record.msg``.
        Process a copy of ``record.msg`` since the some processors modify the
        ``event_dict`` and the ``LogRecord`` will be used for multiple
        formatting runs.
        """
        if isinstance(record.msg, dict):
            msg_repr = self.processor(
                record._logger, record._name, record.msg.copy()
            )
        return msg_repr


def event_dict_to_message(logger, name, event_dict):
    """Passes the event_dict to stdlib handler for special formatting."""
    return (event_dict,), {'extra': {'_logger': logger, '_name': name}}


# Configure structlog stack.
structlog.configure(
    processors=[
        # If log level is too low, abort pipeline and throw away log entry.
        structlog.stdlib.filter_by_level,
        # Add the name of the logger to event dict.
        structlog.stdlib.add_logger_name,
        # Add log level to event dict.
        structlog.stdlib.add_log_level,
        # Perform %-style formatting.
        structlog.stdlib.PositionalArgumentsFormatter(),
        # Add a timestamp in ISO 8601 format.
        structlog.processors.TimeStamper(fmt="iso"),
        # If the "stack_info" key in the event dict is true, remove it and
        # render the current stack trace in the "stack" key.
        structlog.processors.StackInfoRenderer(),
        # If the "exc_info" key in the event dict is either true or a
        # sys.exc_info() tuple, remove "exc_info" and render the exception
        # with traceback into the "exception" key.
        structlog.processors.format_exc_info,
        # add local thread data
        structlog.threadlocal.merge_threadlocal,
        # If some value is in bytes, decode it to a unicode str.
        structlog.processors.UnicodeDecoder(),
        # Add call site parameters.
        structlog.processors.CallsiteParameterAdder(
            [
                structlog.processors.CallsiteParameter.PATHNAME,
                structlog.processors.CallsiteParameter.FUNC_NAME,
                structlog.processors.CallsiteParameter.LINENO,
            ]
        ),
        # Do not include last processor that converts to a string for stdlib
        # since we leave that to the handler's formatter.
        event_dict_to_message,
    ],
    context_class=dict,
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)


def get_logger(
    name='anonymous logger',
    log_file_path='logfile.log',
    log_level='DEBUG',
):
    """
    factory method to create logger instance
    :param name: the new logger name
    :param log_file_path: logfile path (Usually should be in
        `/var/log/PROJECT_NAME/SERVICE_NAME` )
    :param log_level: minimum log level to be appear
    :return: logger instance
    """
    # Set stdout stream
    handler_stream = logging.StreamHandler(sys.stdout)
    handler_stream.setFormatter(
        ProcessorFormatter(processor=structlog.dev.ConsoleRenderer(pad_event=4))
    )
    handler_stream.setLevel(logging.getLevelName(log_level.upper()))

    # Set file stream
    # And use a FileHandler with a JSONRenderer.
    handler_file = logging.FileHandler(log_file_path, encoding='utf-8')
    handler_stream.setLevel(logging.getLevelName(log_level.upper()))
    handler_file.setFormatter(
        ProcessorFormatter(processor=structlog.processors.JSONRenderer())
    )
    _logger = structlog.get_logger(name)
    _logger.setLevel(logging.DEBUG)
    _logger.addHandler(handler_stream)
    _logger.addHandler(handler_file)
    return _logger
