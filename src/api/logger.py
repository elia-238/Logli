import uuid
from functools import wraps

import flask
import structlog

from logli import get_logger


def logit(func):
    @wraps(func)
    def inner():
        structlog.threadlocal.clear_threadlocal()
        structlog.threadlocal.bind_threadlocal(
            view=flask.request.path,
            peer=flask.request.access_route[0],
            transaction_id=str(uuid.uuid4()),
            request_data=flask.request.json,
        )
        return func()

    return inner


logger = get_logger('api')
