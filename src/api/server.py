import random

import flask

from api import (
    logger,
    logit,
    drink_beer,
    write_logs,
)

app = flask.Flask(__name__)


@app.route("/login", methods=["POST", "GET"])
@logit
def login():
    logger.info("user logged in", user="test-user")
    write_logs()
    return flask.make_response("logged in!", 200)


@app.route("/logout", methods=["GET"])
@logit
def logout():
    logger.info(
        "user logged out",
        data={'app': 'Netflix', 'priority': 'high - very high'}
    )
    drink_beer(random.randint(0, 1000))
    return flask.make_response("logged in!", 200)


if __name__ == "__main__":
    app.run("0.0.0.0")
