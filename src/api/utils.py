from api.logger import logger


def drink_beer(num: int):
    logger.info(f'drink beer - {num}')


def write_logs():
    logger.debug('Oops ')
    logger.info('I did again')
    logger.warn('I played with your heart')
    logger.error('Got lost in the game')
    logger.critical('Oh baby, baby')
    try:
        raise KeyError('Oops, you think I\'m in love')
    except KeyError as e:
        logger.exception(e, exec=e)
