# Logli - wrapper for python logger

Logli is small logger that can handle all microservices needs.  
All logs appear with all the necessary information both on the screen (`sys.stdout`) and are also written to a file in
Json format.  
You can change the file path, logger name, and log level in the `get_logger` function.

Every service should have `logger.py` file with decorator that indicate which information fields the service needs to
appear on his logs:  
For example, for service called `api` with logger file `logger.py`:

```python
import flask
import uuid
from functools import wraps
from logli import get_logger

import structlog


def logit(func):
    @wraps(func)
    def inner():
        structlog.threadlocal.clear_threadlocal()
        structlog.threadlocal.bind_threadlocal(
            view=flask.request.path,
            peer=flask.request.access_route[0],
            transaction_id=str(uuid.uuid4()),
            request_data=flask.request.json,
        )
        return func()

    return inner


logger = get_logger('api')
```

And from now on for each log of the API service these fields will appear in each row of the log.  
In the server side, the logger should be called with:

```python
from api.logger import logger

logger.info('nice job!')
``` 